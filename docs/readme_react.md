<head>
    <link rel="stylesheet" href="./docs.css">
</head>
<article> 
    <header> 
        <h1> Boilerplate Docs - React </h1>
        <section>
            <h3> What is React? </h3>
            <p> 
                React is an open-source, front end, JavaScript library for building user interfaces or UI components and encourages the creation of reusable UI components. React is created by Facebook.
            </p>
        </section>
    </header>
    <main>
    </main>
    <aside>
        <section>
            <h3> Learn more about react </h3>
            <a class="bold" href="https://reactjs.org/docs/getting-started.html"> Click here </a> to visit React Website.
        </section>
    </aside>
    <hr>
    <footer>
        <h3> Documentation Overview </h3>
        <p> 
            <a class="bold" href="./readme.md"> Click here </a> to get back to the Documentation Overview.
        </p>
    </footer>
</article>