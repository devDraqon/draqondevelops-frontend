<head>
    <link rel="stylesheet" href="./docs.css">
</head>
<article> 
    <header> 
        <h1> Boilerplate Docs - Webpack </h1>
        <section>
            <h3> What is Webpack? </h3>
            <p> 
                webpack is an open-source JavaScript module bundler. It is made primarily for JavaScript, but it can transform front-end assets such as HTML, CSS, and images if the corresponding loaders are included.
            </p>
        </section>
    </header>
    <main>
    </main>
    <aside>
        <section>
            <h3> Learn more about Webpack </h3>
            <a class="bold" href="https://webpack.js.org/concepts/"> Click here </a> to visit Webpacks Website.
        </section>
    </aside>
    <hr>
    <footer>
        <h3> Documentation Overview </h3>
        <p> 
            <a class="bold" href="./readme.md"> Click here </a> to get back to the Documentation Overview.
        </p>
    </footer>
</article>