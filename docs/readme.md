<head>
    <link rel="stylesheet" href="./docs.css">
</head>
<article>
    <header>
        <h1> Boilerplate Docs - Overview</h1>
    </header>
    <main>
        <section>
            <h2> Documents Overview </h2>
            <ul>
                <li> <a href="./readme_npm.md"> Npm readme file </a> </li>
                <li> <a href="./readme_git.md"> Git readme file </a> </li>
                <li> <a href="./readme_webpack.md"> Webpack readme file </a> </li>
                <li> <a href="./readme_babel.md"> Babel readme file </a> </li>
                <li> <a href="./readme_typescript.md"> Typescript readme file </a> </li>
                <li> <a href="./readme_react.md"> React readme file </a> </li>
                <li> <a href="../readme.md"> Root Folder readme file </a> </li>
            </ul>
        </section>
    </main>
</article>