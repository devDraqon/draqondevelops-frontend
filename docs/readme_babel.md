<head>
    <link rel="stylesheet" href="./docs.css">
</head>
<article> 
    <header> 
        <h1> Boilerplate Docs - Babel </h1>
        <section>
            <h3> What is Babel? </h3>
            <p> 
                Babel is a Javascript compiler that is mainly used to transpile newer Ecmascript versions down to a backwards compatible version of JavaScript (ES5).
                Its also possible to transpile JSX syntax into ES5.
            </p>
        </section>
    </header>
    <main>
    </main>
    <aside>
        <section>
            <h3> Learn more about Babel </h3>
            <a class="bold" href="https://babeljs.io/docs/en/"> Click here </a> to visit Babels Website.
        </section>
    </aside>
    <hr>
    <footer>
        <h3> Documentation Overview </h3>
        <p> 
            <a class="bold" href="./readme.md"> Click here </a> to get back to the Documentation Overview.
        </p>
    </footer>
</article>