import React from "react";
import Router from "../router/Router";
import {CookieBanner} from "draqon-cookie-consent";
import cookies from "/cookies.json";
import "./App.scss";

interface AppProps {

}

const App = ({}: AppProps) => {

    return(
        <div className="app">
            <Router />
            <CookieBanner optionalLogoSrc="https://files.draqondevelops.com/images/logodragon.png" cookies={cookies} hasRouter={true} />
        </div>
    )
}

export default App;
 
