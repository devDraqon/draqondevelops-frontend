import React from "react";
import {Headline, Paragraph} from "draqon-component-library";
import "./Copyright.scss";

interface CopyrightProps {

}

const Copyright = ({}: CopyrightProps) => {

    return (
        <div className="copyright">
            <Paragraph size="small"> Copyright Niklas Müller © 2019-2021 </Paragraph>
        </div>
    )
}

export default Copyright;