import React from "react";
import {Headline, Paragraph, Link, List, Code} from "draqon-component-library";
import "./PackageInstallationGuide.scss";

interface PackageInstallationGuideProps {
    requirements: any;
    repolink: string;
    command__import: string;
    command__installation: string;
}

const PackageInstallationGuide = ({requirements, repolink, command__import, command__installation}: PackageInstallationGuideProps) => {
    return(
        <section className="packageInstallationGuide">
            <Headline size={"xlarge"}> NPM Package Installation Guide </Headline>
                <section>
                    <Headline size="medium"> I) Requirements </Headline>
                    <List classList={["packageInstallationGuide__list-install"]} isOrdered={false} listItems={
                        requirements.map((requirement: string) => 
                        (requirement === "node") ? <div className="item">
                            <Paragraph size="medium"> You must have npm and node installed. </Paragraph>
                        </div>
                        : (requirement === "react") ? <div className="item">
                            <Paragraph size="medium"> Your project must use React </Paragraph>
                        </div>
                        : (requirement === "es5") ? <div className="item">
                            <Paragraph size="medium"> You must be able to <b>import</b>, which means that you should use the Ecmascript implementation of Javascript (ES5 or higher).  </Paragraph>
                        </div> : null )
                    } />
                </section>
                <section>
                    <section>
                        <Headline size="medium"> II) Installation </Headline>
                        <List classList={["packageInstallationGuide__list-install"]} isOrdered={true} listItems={[
                            <div className="item">
                                <Paragraph size="medium"> Open your terminal and navigate to your project folder. </Paragraph>
                            </div>,
                            <div className="item">
                                <Paragraph size="medium"> Enter the following command to install this package. </Paragraph>
                                <Code isInline={false}>{command__installation}</Code>
                            </div>,
                        ]} />
                    </section>
                    <section>
                        <Headline size={"medium"}> III) Importing Components </Headline>
                        <Code isInline={false}>{command__import}</Code>
                    </section>
                    <section>
                        <Headline size={"medium"}> IV) Source Code <i><small>(for developers)</small></i> </Headline>
                        <Paragraph size="medium">
                            Feel free to fork and edit this codebase as much as you like :) <br/>
                            <Link isReactLink={false} url={repolink} target="_blank">Click here</Link> to visit the source code of this project.
                        </Paragraph>
                    </section>
            </section>
        </section>
    )
}

export default PackageInstallationGuide;
 
