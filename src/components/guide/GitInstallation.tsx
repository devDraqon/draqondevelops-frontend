import React from "react";
import {Headline, Paragraph, Link, List, Code} from "draqon-component-library";
import "./GitInstallation.scss";

interface GitInstallationGuideProps {
    repolink: string;
    showCompleteGuide?: boolean;
}


const GitInstallationGuide = ({repolink, showCompleteGuide}: GitInstallationGuideProps) => { 
    return(
        <section className="gitInstallation">
            <Headline size={"xlarge"}> Git Repository Installation Guide </Headline>
            <section>
                <Headline size="medium"> I) Requirements </Headline>
                <List classList={["gitInstallation__list-install"]} isOrdered={false} listItems={[
                    <div className="item">
                        <Paragraph size="medium"> <b>git</b> must be installed locally on your machine </Paragraph>
                    </div> ]}
                />
            </section>
            <section>
                <Headline size="medium"> II) Cloning the Repistory </Headline>
                <List classList={["gitInstallation__list-install"]} isOrdered={true} listItems={[
                    <div className="item">
                        <Paragraph size="medium"> Open your terminal and navigate to your new repositories parent folder. </Paragraph>
                    </div>,
                    <div className="item">
                        <Paragraph size="medium"> Enter the following command to clone the repository. </Paragraph>
                        <Code isInline={false}>{"git clone "+ repolink +".git"}</Code>
                    </div>,
                ]} />
            </section>
            {(showCompleteGuide) ? 
            <section>
                <Headline size="medium"> III) Common Git Commands </Headline>
                <Headline size="small"> Branches </Headline>
                <List classList={["gitInstallation__list-install"]} isOrdered={false} listItems={[
                    <div className="item">
                        <Paragraph size="medium"> Change to new or existing branch. </Paragraph>
                        <Code isInline={false}>{"git checkout branchname"}</Code>
                    </div>,
                    <div className="item">
                        <Paragraph size="medium"> List all branches. </Paragraph>
                        <Code isInline={false}>{"git branch"}</Code>
                    </div>,
                    <div className="item">
                        <Paragraph size="medium"> List previous commits of this branch. </Paragraph>
                        <Code isInline={false}>{'git log'}</Code>
                    </div>,
                ]} />
                <Headline size="small"> Commits </Headline>
                <List classList={["gitInstallation__list-install"]} isOrdered={false} listItems={[
                    <div className="item">
                        <Paragraph size="medium"> Add changes of any file to current commit. </Paragraph>
                        <Code isInline={false}>{'git add ./src/to/file.txt'}</Code>
                    </div>,
                    <div className="item">
                        <Paragraph size="medium"> Undo changes of any file in current commit. </Paragraph>
                        <Code isInline={false}>{'git reset ./src/to/file.txt'}</Code>
                    </div>,
                    <div className="item">
                        <Paragraph size="medium"> Reset to last commit. </Paragraph>
                        <Code isInline={false}>{"git stash"}</Code>
                    </div>,
                    <div className="item">
                        <Paragraph size="medium"> Create new commit. </Paragraph>
                        <Code isInline={false}>{'git commit -m "commit message"'}</Code>
                    </div>,
                    ]} />
                    <Headline size="small"> Pushing and Pulling </Headline>
                    <List classList={["gitInstallation__list-install"]} isOrdered={false} listItems={[    
                    <div className="item">
                        <Paragraph size="medium"> Push changes to git. </Paragraph>
                        <Code isInline={false}>{'git push'}</Code>
                    </div>,
                    <div className="item">
                        <Paragraph size="medium"> Pull changes from <b>this branch</b> from git. </Paragraph>
                        <Code isInline={false}>{'git pull'}</Code>
                    </div>,
                    <div className="item">
                        <Paragraph size="medium"> Pull changes from <b>this repository</b> from git. </Paragraph>
                        <Code isInline={false}>{'git fetch'}</Code>
                    </div>,
                ]} />
            </section> : null}
        </section>
    )
}

export default GitInstallationGuide;
 
