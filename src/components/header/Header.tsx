import React, { useEffect, useState } from "react";
import { Link as FixLink } from "react-router-dom";
import { useMediaQuery } from 'react-responsive';
import { Image, Modal, Icon, Headline, Flex, Link, Paragraph } from "draqon-component-library";
import "./Header.scss";

interface HeaderProps {

}

const Header = ({ }: HeaderProps) => {
    const isMobileScreen = useMediaQuery({ query: '(max-width: 640px)' })
    const [navMenuShown, toggleNavMenuShown] = useState(false);
    const [navMenuItem, setNavMenuItem] = useState("none");

    const toggleNavigation = (item?: string) => {
        if (item === "close") {
            toggleNavMenuShown(false);
            setNavMenuItem("none");
        }
        else if(item) {
            console.log(item === navMenuItem ? "none" : item !== "none" ? true : false)
            if(item === navMenuItem ? "none" : item !== "none" ? true : false) toggleNavMenuShown(true);
        } 
        else toggleNavMenuShown(!navMenuShown);
    }

    const updateNavMenuItem = (item: string) => {
        setNavMenuItem(item === navMenuItem ? "none" : item);
    }

    useEffect(() => {
        //if(navMenuItem === "none" && navMenuShown) toggleNavigation();    /* enabling this line will close the modal when disabling a selection */
    }, [navMenuItem])

    const Navigation: any = () => {
        return [
            <nav key="0" className="header__navigation">
                {
                    (!isMobileScreen)
                        ? <Flex direction="horizontal">
                            <FixLink className="link" target="_self" to="/"> <Image src="https://files.draqondevelops.com/images/home.png" alt="logodragon" /> </FixLink>
                            <span className="header__navigation-button" onClick={() => {updateNavMenuItem("general"); toggleNavigation("general"); }}><Paragraph classList={navMenuShown && navMenuItem === "general" ? ["highlight"] : []} size="small"> General </Paragraph></span>
                            <span className="header__navigation-button" onClick={() => {updateNavMenuItem("projectsweb"); toggleNavigation("projectsweb");}}><Paragraph classList={navMenuShown && navMenuItem === "projectsweb" ? ["highlight"] : []} size="small"> Web Dev </Paragraph></span>
                            <span className="header__navigation-button" onClick={() => {updateNavMenuItem("projectsgame"); toggleNavigation("projectsgame");}}><Paragraph classList={navMenuShown && navMenuItem === "projectsgame" ? ["highlight"] : []} size="small"> Game Dev </Paragraph></span>
                        </Flex>
                        : <Flex direction="horizontal">
                            <Icon alt="icon" callbackOnClick={toggleNavigation} classList={["header__navigation-image"]} src="https://files.draqondevelops.com/images/menu.png" />
                            <FixLink className="link" target="_self" to="/"> <Image src="https://files.draqondevelops.com/images/home.png" alt="logodragon" /> </FixLink>
                        </Flex>
                }
            </nav>
        ].map((item) => item)
    }

    const ProjectsWebLinks = () =>
        <div className={isMobileScreen ? "navMenu__links navMenu__links-mobile" : "navMenu__links"}>
            <Flex classList={["navMenu__links-groups"]} direction="horizontal">
                <Flex direction="vertical">
                    <Headline size="small"> Boilerplates </Headline>
                    <Flex classList={["navMenu__linkgroup"]} direction="horizontal">
                        <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/boilerplates/webpack"> Webpack Boilerplate </FixLink>
                        <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/boilerplates/express"> Express Boilerplate </FixLink>
                        <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/boilerplates/rollup"> Rollup Boilerplate </FixLink>
                    </Flex>
                </Flex>
                <Flex direction="vertical">
                    <Headline size="small"> NPM Packages </Headline>
                    <Flex classList={["navMenu__linkgroup"]} direction="horizontal">
                        <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/packages/react-component-library"> Draqon-Component-Library </FixLink>
                        <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/packages/react-cookie-consent"> Draqon-Cookie-Consent </FixLink>
                        <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/packages/react-media-player"> Draqon-Media-Player </FixLink>
                        <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/packages/es6-modules"> Draqon-Modules </FixLink>
                    </Flex>
                </Flex>
            </Flex>
        </div>

    const ProjectsGameLinks = () =>
        <div className={isMobileScreen ? "navMenu__links navMenu__links-mobile" : "navMenu__links"}>
            <Flex classList={["navMenu__links-groups"]} direction="horizontal">
                <Flex classList={["navMenu__linkgroup"]} direction="vertical">
                    <Headline size="small"> GTA San Andreas </Headline>
                    <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/gamedev/gta-sa-modded-edition"> GTA-SA Modded Edition </FixLink>
                </Flex>
                <Flex classList={["navMenu__linkgroup"]} direction="vertical">
                    <Headline size="small"> Blender Models </Headline>
                    <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/gamedev/blender-models"> Blender Models  </FixLink>
                </Flex>
            </Flex>
        </div>

    const GeneralLinks = () =>
        <div className={isMobileScreen ? "navMenu__links navMenu__links-mobile" : "navMenu__links"}>
            <Flex classList={["navMenu__links-groups"]} direction="horizontal">
                <Flex classList={["navMenu__linkgroup"]} direction="vertical">
                    <Headline size="small"> General </Headline>
                    <Flex direction="horizontal">
                        <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/"> Home Page </FixLink>
                        <FixLink onClick={() => toggleNavigation("close")} className="link" target="_self" to="/contact"> Contact Me </FixLink>
                    </Flex>
                </Flex>
            </Flex>
        </div>


    const Logo = () => {
        return (
            <div className="header__logo">
                <Flex direction="horizontal">
                    <Image src="https://files.draqondevelops.com/images/logodragon.png" alt="logodragon" />
                    <Headline size="large">Draqon<br />Develops</Headline>
                </Flex>
            </div>
        )
    }

    const ModalSelectionFiller = () => 
        <div className="selection-filler">
            <Paragraph size="small"> Please choose a section ...</Paragraph>
        </div>

    const ModalSelection = () => 
    (navMenuItem === "projectsweb")
    ? ProjectsWebLinks() 
    : (navMenuItem === "projectsgame")
        ? ProjectsGameLinks()
        : (navMenuItem === "general")
            ? GeneralLinks() 
            : (navMenuShown && isMobileScreen) ? <ModalSelectionFiller /> : null;


    return (
        <header className="header">
            <Flex classList={["header__head"]} direction="vertical">
                <Navigation />
                <Logo />
            </Flex>
            {
                (navMenuShown && isMobileScreen)
                    ?   <Modal callbackOnClose={() => toggleNavigation("close")} key="1" position="top" classList={["navMenu"]}>
                            <div className="navMenu__selection">
                                <Headline classList={["navMenu__selection-title"]} size="medium"> Sections </Headline>
                                <Flex direction="horizontal">
                                    <span className="header__navigation-button" onClick={() => updateNavMenuItem("general")}><Paragraph classList={navMenuShown && navMenuItem === "general" ? ["highlight"] : []} size="medium"> General </Paragraph></span>
                                    <span className="header__navigation-button" onClick={() => updateNavMenuItem("projectsweb")}><Paragraph classList={navMenuShown && navMenuItem === "projectsweb" ? ["highlight"] : []} size="medium"> Web Dev </Paragraph></span>
                                    <span className="header__navigation-button" onClick={() => updateNavMenuItem("projectsgame")}><Paragraph classList={navMenuShown && navMenuItem === "projectsgame" ? ["highlight"] : []} size="medium"> Game Dev </Paragraph></span>
                                </Flex>
                                <ModalSelection />
                            </div>
                        </Modal>
                    :  <ModalSelection />
            }
        </header>
    )
}

export default Header;

