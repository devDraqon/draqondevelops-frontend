import React from "react";
import {HashRouter, Route as Page, Switch as PageSwitch} from "react-router-dom";
import Header from "../header/Header";
import Footer from "../footer/Footer";
import {Spacer} from "draqon-component-library";

import Empty from "../../pages/empty/Empty";
import Index from "../../pages/index/Index";
import Contact from "../../pages/contact/Contact";
import BoilerplateWebpack from "../../pages/boilerplateWebpack/BoilerplateWebpack";
import BoilerplateRollup from "../../pages/boilerplateRollup/BoilerplateRollup";
import BoilerplateExpress from "../../pages/boilerplateExpress/BoilerplateExpress";
import PackageComponentLibrary from "../../pages/packageComponentLibrary/PackageComponentLibrary";
import PackageMediaPlayer from "../../pages/packageMediaPlayer/PackageMediaPlayer";
import PackageEsModules from "../../pages/packageEsModules/PackageEsModules";
import PackageCookieConsent from "../../pages/packageCookieConsent/PackageCookieConsent";
import GameDevGTASA from "../../pages/gamedevGTASA/GamedevGTASA";
import GameDevBlenderModels from "../../pages/gamedevBlenderModels/GamedevBlenderModels";
import "./Router.scss";

interface RouterProps {

}

const Router = ({}: RouterProps) => {
    const PageRouter = () => (
        <div className="page"> 
            <PageSwitch>
                <Page exact path="/"> <Index /> </Page>
                <Page path="/boilerplates/webpack"> <BoilerplateWebpack /> </Page>
                <Page path="/boilerplates/rollup"> <BoilerplateRollup /> </Page>
                <Page path="/boilerplates/express"> <BoilerplateExpress /> </Page>
                <Page path="/packages/react-component-library"> <PackageComponentLibrary /> </Page>
                <Page path="/packages/react-media-player"> <PackageMediaPlayer /> </Page>
                <Page path="/packages/react-cookie-consent"> <PackageCookieConsent /> </Page>
                <Page path="/packages/es6-modules"> <PackageEsModules /> </Page>
                <Page path="/gamedev/gta-sa-modded-edition"> <GameDevGTASA /> </Page>
                <Page path="/gamedev/blender-models"> <GameDevBlenderModels /> </Page>
                <Page path="/contact"> <Contact /> </Page>
                <Page> <Empty /> </Page>
            </PageSwitch>
        </div>
    )

    return(
        <div className="router">
            <HashRouter>
                    <Spacer>
                        <Header />
                        <PageRouter /> 
                    </Spacer>
                    <Footer />
            </HashRouter>
        </div>
    )
}

export default Router;
 
