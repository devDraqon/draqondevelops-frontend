import React from "react";
import {Headline, Flex} from "draqon-component-library";
import Copyright from "../copyright/Copyright";
import {useMediaQuery} from "react-responsive";
import "./Footer.scss";

interface FooterProps {

}

const Footer = ({}: FooterProps) => {
    const isMobileScreen = useMediaQuery({ query: '(max-width: 640px)' })
    return(
        <footer className="footer">
            <Flex direction={isMobileScreen ? "vertical" : "horizontal"}>
                <Headline size="large"> Footer Headline </Headline>
                <Copyright />
            </Flex>
        </footer>
    )
}

export default Footer;
 
