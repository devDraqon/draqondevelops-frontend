import React from "react";
import {Headline, Paragraph} from "draqon-component-library";
import "./Empty.scss";

interface EmptyProps {

}

const Empty = ({}: EmptyProps) => {
    return(
        <main className="empty">
            <Headline size={"xxlarge"}> Page not found </Headline>
            <Paragraph size="large"> 
                Describe this page . . .
            </Paragraph>
        </main>
    )
}

export default Empty;
 
