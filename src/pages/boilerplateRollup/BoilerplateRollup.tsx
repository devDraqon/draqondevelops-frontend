import React from "react";
import {Headline, Paragraph, List} from "draqon-component-library";
import "./BoilerplateRollup.scss";
import GitInstallationGuide from "../../components/guide/GitInstallation";

interface BoilerplateRollupProps {

}

const BoilerplateRollup = ({}: BoilerplateRollupProps) => {
    return(
        <main className="boilerplateRollup">
            <Headline size={"xxlarge"}> Rollup Boilerplate </Headline>
            <Paragraph size="large"> 
                Boilerplate to quickly create new React Component Libraries.
            </Paragraph>

            <section>
                <Headline size={"large"}> Feature List </Headline>
                <List isOrdered={false} listItems={[
                    "React Support",
                    "Storybook Support",
                    "Typescript Support",
                    "Distributable Static Storybooks",
                    "Barrel Exports for Libraries",
                    "Development Server",
                    "ES Syntax",
                    "ES Modules",
                    "Sass Stylesheets",
                    "NPM Packages"
                ]} />
            </section>

            <GitInstallationGuide repolink="https://gitlab.com/devDraqon/boilerplate-rollup"/>

        </main>
    )
}

export default BoilerplateRollup;
 
