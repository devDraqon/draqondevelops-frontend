import React from "react";
import {Headline, Paragraph, Link, List, Code} from "draqon-component-library";
import "./PackageEsModules.scss";
import PackageInstallationGuide from "../../components/guide/PackageInstallation";
import GitInstallationGuide from "../../components/guide/GitInstallation";

interface PackageEsModulesProps {

}

const command__installation = 'npm install --save draqon-modules';
const command__import = 'import {GetCookie} from draqon-modules';

const PackageEsModules = ({}: PackageEsModulesProps) => {
    return(
        <main className="packageEsModules">

            <Headline size={"xxlarge"}> ES6 Function Library </Headline>
            <Paragraph size="large">
                Public ES6 Function Library (Distributed over npm).
            </Paragraph>


            <PackageInstallationGuide 
                repolink="https://gitlab.com/devDraqon/draqon-modules"
                requirements={["node", "es5"]} 
                command__import={command__import} 
                command__installation={command__installation} 
            />

            <GitInstallationGuide repolink="https://gitlab.com/devDraqon/draqon-modules"/>

        </main>
    )
}

export default PackageEsModules;
 
