import React from "react";
import {Headline, Paragraph, List, Code} from "draqon-component-library";
import "./BoilerplateWebpack.scss";
import GitInstallationGuide from "../../components/guide/GitInstallation";

interface BoilerplateWebpackProps {

}

const BoilerplateWebpack = ({}: BoilerplateWebpackProps) => {
    return(
        <main className="boilerplateWebpack">
            <Headline size={"xxlarge"}> Webpack Boilerplate </Headline>
            <Paragraph size="large">
                Boilerplate to quickly create new React Applications.
                This repository has 6 production ready branches, reaching from Vanilla to Typescript with React and Storybook.
            </Paragraph>

            <section>
                <Headline size={"large"}> Feature List </Headline>
                <List isOrdered={false} listItems={[
                    "Development Server",
                    "ES Syntax",
                    "ES Modules",
                    "Sass Stylesheets",
                    "NPM Packages",
                    "Production Build",
                    "Ranging from Vanilla Javascript to React + Typescript + Storybook"
                ]} />
            </section>

            <GitInstallationGuide repolink="https://gitlab.com/devDraqon/boilerplate-webpack"/>
        
        </main>
    )
}

export default BoilerplateWebpack;
 
