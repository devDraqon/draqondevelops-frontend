import React from "react";
import {Headline, Paragraph} from "draqon-component-library";
import "./Contact.scss";

interface ContactProps {

}

const Contact = ({}: ContactProps) => {
    return(
        <main className="contact">
            <Headline size={"xxlarge"}> Contact Me! </Headline>
            <Paragraph size="large"> 
                Describe this page . . .
            </Paragraph>
        </main>
    )
}

export default Contact;
 
