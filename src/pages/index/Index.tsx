import React from "react";
import {Headline, Paragraph} from "draqon-component-library";
import "./Index.scss";

interface IndexProps {

}

const Index = ({}: IndexProps) => {
    return(
        <main className="index">
            <Headline size={"xxlarge"}> Creating digital Worlds<br/>with passion </Headline>
            <Paragraph size="large"> 
                Hello and Welcome to DraqonDevelpos.com, a Developers Portal.
                I am Draqon, a Freelancing Fullstack Web and Game Developer and am currently looking for work.
            </Paragraph>
        </main>
    )
}

export default Index;
 
