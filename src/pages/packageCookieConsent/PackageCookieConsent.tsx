import React from "react";
import {Link as FixLink} from "react-router-dom";
import {Headline, Paragraph, Link, Code, List} from "draqon-component-library";
import "./PackageCookieConsent.scss";
import PackageInstallationGuide from "../../components/guide/PackageInstallation";
import GitInstallationGuide from "../../components/guide/GitInstallation";

interface PackageCookieConsentProps {

}

const command__installation = 'npm install --save draqon-cookie-consent';
const command__import = 'import CookieBanner from draqon-cookie-consent';

const PackageCookieConsent = ({}: PackageCookieConsentProps) => {
    return(
        <main className="packageCookieConsent">
            <Headline size={"xxlarge"}> React Cookie Consent </Headline>
            <Paragraph size="large">
                Public React Cookie Consent Banner (Distributed over npm).
            </Paragraph>



            <section>
                <Headline size={"xlarge"}> Live Demo </Headline>
                <Paragraph size="medium">
                    <Link isReactLink={false} url="https://storybook.draqondevelops.com/react-cookie-consent" target="_blank">Click here</Link> to visit the live version of this cookie consent banner.
                </Paragraph>
            </section>

            <section className="sectionad">
                <div className="sectionad__content">
                <Headline size="medium"> Boilerplate Advertisement </Headline>
                    <Paragraph size="medium">
                        It can be truly annoying at times to create a development environment for javascript projects.
                        Exactly this is why I created the ultimate Javascript Boilerplate Package which consists out of 6 branches.
                        &nbsp;<FixLink className="link" target={"_self"} to="/boilerplates/webpack">Click here</FixLink> to visit my Javascript Boilerplate.
                    </Paragraph>
                    <List isOrdered={false} listItems={[
                        "Vanilla", 
                        "Vanilla React", 
                        "Vanilla React Storybook", 
                        "Typescript", 
                        <span> <b> Typescript React </b> <i> (Recommended)</i> </span>, 
                        "Typescript React Storybook"
                    ]} />
                    <Paragraph size="medium">
                        So, before you use this package in production, be sure to test it somewhere first.
                    </Paragraph>
                    </div>
            </section>

            <PackageInstallationGuide 
                        repolink="https://gitlab.com/devDraqon/draqon-cookie-consent"
                        requirements={["node", "es5", "react"]} 
                        command__import={command__import} 
                        command__installation={command__installation} 
            />

            <GitInstallationGuide repolink="https://gitlab.com/devDraqon/draqon-modules"/>


        </main>
    )
}

export default PackageCookieConsent;
 
