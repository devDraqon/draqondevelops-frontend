import React from "react";
import {Headline, Paragraph, List} from "draqon-component-library";
import GitInstallationGuide from "../../components/guide/GitInstallation";
import "./BoilerplateExpress.scss";

interface BoilerplateExpressProps {

}

const BoilerplateExpress = ({}: BoilerplateExpressProps) => {
    return(
        <main className="boilerplateExpress">
            <Headline size={"xxlarge"}> Express Boilerplate </Headline>
            <Paragraph size="large"> 
                Boilerplate to quickly create new Express API's.
            </Paragraph>

            <section>
                <Headline size={"large"}> Feature List </Headline>
                <List isOrdered={false} listItems={[
                    "CommonJS Support",
                    "ExpressJS Support",
                    "Jade Support",
                    "Easily Handleable CORS reqests",
                    "Supports GET, POST, OPTIONS, PUT, DELETE Requests ...",
                    "Can easily handle MongoDB (with Mongoose) and other Databases",
                    "Can serve as static file server",
                    "Simple handling for routes",
                ]} />
            </section>

            <GitInstallationGuide repolink="https://gitlab.com/devDraqon/boilerplate-express"/>

        </main>
    )
}

export default BoilerplateExpress;
 
