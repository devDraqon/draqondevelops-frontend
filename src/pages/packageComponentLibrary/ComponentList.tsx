import React from "react";
import {Link as FixLink} from "react-router-dom";
import { Headline, Paragraph, Code, Link, List } from "draqon-component-library";
import "./PackageComponentLibrary.scss";
import PackageInstallationGuide from "../../components/guide/PackageInstallation";
import GitInstallationGuide from "../../components/guide/GitInstallation";
import json from "./Components.json";

interface ComponentListProps {
    showAllVariants?: boolean;
}

const ComponentList = ({showAllVariants}: ComponentListProps) => {
    const list: any = json.list;
    return(
        <List isOrdered={false} listItems={
            list.map((component: any) => 
                <div className="item">
                <Headline size="small"> {component.name} </Headline>
                {showAllVariants ? component.variants.map((variant: string) => 
                    <List isOrdered={false} listItems={[
                        <Paragraph size="medium">{variant} </Paragraph>,
                    ]} />
                ) : null}
            </div>,
            )
        } />
    )
}

export default ComponentList;