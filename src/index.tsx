import React from "react";
import ReactDOM from "react-dom";
import App from "./components/app/App";
import "./index.scss";

document.getElementById("wrapper") ? ReactDOM.render(<App />, document.getElementById("wrapper")) : null;
